# covid19-des-model

A DES (Discreet-Event-Simulation) model of COVID-19 across populations. All data is parameterized through YAML files and Excel worksheets, so there is nothing specific here for COVID-19.

## Viral modeling through a DES

Mathematicians and Statisticians typically model the spread of a virus in a population using
steady-state differential equations, solved either analytically or through numerical methods.
The power of these models is that the results can be determined nearly instantly on modern 
computers. The weakness of them is as follows:

1. Changes to the parameters and assumptions may require re-solving of the equation
2. As the environmental conditions change, so might the parameters, such that the equation
becomes a union of discreet time steps. 

Another approach is to simulate the population stepwise through time. The
population is divided into  groups, each representing a different state of virus
infection. For each step in time, to each group, one or more state transition 
formulae are applied, resulting in a portion of each group either 
retaining its state or being assigned to a new group. Once all the 
group states are updated, a test is made to see if a configurable
steady-state condition has been reached, and if so, the simulation completes. 
Otherwise, the the states are updated and the next step in time is simulated.

To model a virus' stages more precisely, each group, representing a possible viral state, 
such as "infected-asymptomatic", "infected-symptomatic",
may have one or more historical sub-groups. The starting state will typically be
"vulnerable", though it may also be useful to start a simulation with an already infected
or partly immune population. Terminal states include "recovered-immune" and "dead".
(A state such as "recovered-still-vulnerable" is simply "vulnerable"). 

For each step in time, to each sub-group a probability function is applied.  
Each function expresses the question "How many of this population will change state?"
The function's inputs include the population group of interest, the current time-step, 
all the other populations, and a configurable parameterized values. For instance, 
to the group "infected", sub-group "day2" will be applied a function "chance-recovered", 
and the output of that function will be _subtracted_ from the "day2" subgroup and 
_added_ to the "recovered" group (which has no day-type sub-group). After all other
functions are applied, a "default" function is used, which in this case would move
the remaining "day2" population to the "day3" population.

These state-transtion functions are the meat of the simulation and require 
special attention.

As the state of the population evolves, and different environmental conditions change,
ie, social-distancing rules are applied, the formulae and parameters may change. 
To accommodate this, different _phases_ may be configured. Each phase is triggered
at a certain time, and its configuration replaces or supplments the existing one. 


## Implementation

>  "Instead of building a smarter algorithm, build a smarter data structure" -- Gus Baird.

We construct the set of states and paramters using various [YAML](https://yaml.org/) files. 
The YAML format is chosen due to its readability and maintainability. The format
can be converted back and forth to JSON, allowing both automation and interaction with
the web. State is saved through YAML as well. In future work, standard compression libraries
may be used to reduce the size of these state files.

The YAML structure is used to express:

*  The full set of groups and whether each has a sub-group
*  The transition functions to apply to each sub-group
*  Configuration parameters to be used with each transition function
*  The various phases
*  The initial populations
*  The steady-state condition

The copmlete YAML configuration may be specified in a single file, or
across multiple files. Newer structures are deep-merged with older ones. 
The configurations are read in the following order:

1. `config.yaml`
2. `states.yaml`
3. `phase`_`nn`_`.yaml`

Sample files are contained in the `examples/` directory.

